# README #

Writing test class using best practices

### What is this repository for? ###

* The Unit test ensures that it works as expected. Whenever we want to deploy our code (Apex Class/ Trigger) to production, test classes with at least 75% code coverage(the percentage of code working) is mandatory  In the testing framework, your code is tested and the testing code is coded in the sandbox environment, and then deployed to production Org.  The minimum code to be covered is 75% in order to deploy in production from Sandbox.

* Link https://bitbucket.org/dilip-rathore/testclasswithbestpractices/src/master/
* Document Link: https://docs.google.com/document/d/1oLwJEmJUX5n9i_Qoap9HgnRl106GJB2ZWjA2R8forL0/edit#

### How do I get set up? ###

* Summary of set up : Just deploy the above three menifest items (listed below) into your org and you can see the result when you access preview VF Page FolderAccessPage
    - Create an object Booko__c with a number field (Price)
    - trigger :- BookTrigger 
    - Apex Class :- BookTriggerHelper
    - Test Class :- BookTriggerHelperTest

### Contribution guidelines ###

* Code review : Rajat Jain

### Who do I talk to? ###

* Repo owner or admin : Dilip Rathore

* Modified By : Akhilesh Kumar