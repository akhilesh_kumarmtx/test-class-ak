/*
* Class : BookTriggerHelper
* Test Class: BookTriggerHelperTest
* Objective: Class written to give 10% discount on price
* Date(Created): 9/17/2020
*/
public class BookTriggerHelper{
 public static void applyDiscount(Book__c[] books) {
     for (Book__c b :books){
     	b.Price__c *= 0.9;
   }
 }
}